#![allow(unused_imports)]
#![allow(unused_macros)]

extern crate guile_bind;

#[cfg(test)]
mod scm_char {
    use guile_bind::char::*;
    use guile_bind::features::*;
    use guile_bind::scheme::{eval_string, Any, SCM};
    use guile_bind::*;

    #[test]
    fn from_into() {
        scheme::init_guile_for_current_thread();

        assert_eq!('a', char::try_from(SCM::from('a')).unwrap());
        assert_ne!('y', char::try_from(SCM::from('d')).unwrap());
    }

    #[test]
    fn is() {
        scheme::init_guile_for_current_thread();

        assert!(SCM::from(':').is_char());
        assert!(!eval_string::<Any>("'string").is_char());
    }

    #[test]
    fn is_alphabetic() {
        scheme::init_guile_for_current_thread();

        assert!(bool::from(SCM::from('a').is_alphabetic()));
        assert!(!bool::from(SCM::from('1').is_alphabetic()));
    }

    #[test]
    fn is_numeric() {
        scheme::init_guile_for_current_thread();

        assert!(bool::from(SCM::from('6').is_numeric()));
        assert!(!bool::from(SCM::from('m').is_numeric()));
    }

    #[test]
    fn is_whitespace() {
        scheme::init_guile_for_current_thread();

        assert!(bool::from(SCM::from(' ').is_whitespace()));
        assert!(!bool::from(SCM::from('=').is_whitespace()));
    }

    #[test]
    fn is_uppercase() {
        scheme::init_guile_for_current_thread();

        assert!(bool::from(SCM::from('R').is_uppercase()));
        assert!(!bool::from(SCM::from('r').is_uppercase()));
    }

    #[test]
    fn is_lowercase() {
        scheme::init_guile_for_current_thread();

        assert!(bool::from(SCM::from('h').is_lowercase()));
        assert!(!bool::from(SCM::from('H').is_lowercase()));
    }

    #[test]
    fn is_upper_or_lower_case() {
        scheme::init_guile_for_current_thread();

        assert!(bool::from(SCM::from('y').is_upper_or_lower_case()));
        assert!(bool::from(SCM::from('Y').is_upper_or_lower_case()));
        assert!(!bool::from(SCM::from('+').is_upper_or_lower_case()));
    }

    #[test]
    fn general_category() {
        scheme::init_guile_for_current_thread();
        let c = scheme::eval_string::<char>(r"#\c").general_category();

        assert!(c.is_some(), "should be some category");
        assert_eq!(UnicodeGeneralCategory::LowercaseLetter, c.unwrap());
    }

    #[test]
    fn to_uppercase() {
        scheme::init_guile_for_current_thread();

        assert_eq!('E', char::try_from(SCM::from('e').to_uppercase()).unwrap());
        assert_eq!('5', char::try_from(SCM::from('5').to_uppercase()).unwrap());
    }

    #[test]
    fn to_lowercase() {
        scheme::init_guile_for_current_thread();

        assert_eq!('t', char::try_from(SCM::from('T').to_lowercase()).unwrap());
        assert_eq!('3', char::try_from(SCM::from('3').to_lowercase()).unwrap());
    }
}
