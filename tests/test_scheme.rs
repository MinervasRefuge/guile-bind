extern crate guile_bind;

#[cfg(test)]
mod test_scheme {
    use guile_bind::scheme::Any;
    use guile_bind::scheme::SCM;
    use guile_bind::symbol::Symbol;
    use guile_bind::*;

    use std::*;

    #[test]
    fn internal_catch() {
        scheme::init_guile_for_current_thread();

        let truth = cell::Cell::new(false);

        scheme::internal_catch(SCM::<Symbol>::from("test-internal-catch"),
                               &|| {
                                   truth.set(true);
                               },
                               &|_key, _args| {});

        assert!(truth.get(), "Body of catch never ran");
    }

    #[test]
    fn internal_catch_catch() {
        scheme::init_guile_for_current_thread();

        let truth = cell::Cell::new(false);

        scheme::internal_catch(SCM::<Symbol>::from("test-internal-catch"),
                               &|| {
                                   scheme::eval_string::<Any>("(throw 'test-internal-catch)");
                               },
                               &|_key, _args| {
                                   truth.set(true);
                               });

        assert!(truth.get(), "catch for test-internal-catch never ran");
    }

    #[test]
    #[should_panic]
    fn internal_catch_body_panic() {
        scheme::init_guile_for_current_thread();

        scheme::internal_catch(SCM::<Symbol>::from("test-internal-catch"),
                               &|| {
                                   panic!();
                               },
                               &|_key, _args| {});
    }

    #[test]
    #[should_panic]
    fn internal_catch_catch_panic() {
        scheme::init_guile_for_current_thread();

        scheme::internal_catch(SCM::<Symbol>::from("test-internal-catch"),
                               &|| {
                                   scheme::eval_string::<Any>("(throw 'test-internal-catch)");
                               },
                               &|_key, _args| {
                                   panic!();
                               });
    }
}
