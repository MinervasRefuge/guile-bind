#![allow(unused_imports)]
#![allow(unused_macros)]

extern crate guile_bind;

#[cfg(test)]
mod scm_keyword {
    use guile_bind::features::*;
    use guile_bind::keyword::*;
    use guile_bind::scheme::{Any, SCM};
    use guile_bind::symbol::*;
    use guile_bind::*;

    #[test]
    fn from_into() {
        scheme::init_guile_for_current_thread();

        assert!(SCM::<Keyword>::from(scheme::eval_string::<Symbol>("'Metropolis")).is_keyword());
        assert!(SCM::<Keyword>::from("Casablanca".to_string()).is_keyword());
    }

    #[test]
    fn is() {
        scheme::init_guile_for_current_thread();

        assert!(scheme::eval_string::<Keyword>("#:Avatar").is_keyword());
        assert!(!scheme::eval_string::<Any>("'Roberta").is_keyword());
    }

    #[test]
    fn default() {
        scheme::init_guile_for_current_thread();

        assert_eq!(String::default(),
                   String::from(SCM::<Keyword>::default().into_scm::<Symbol>()
                                                         .into_scm::<String>()));
    }
}
