#![allow(unused_imports)]

extern crate guile_bind;

#[cfg(test)]
mod scm_bool {
    use guile_bind::bool::*;
    use guile_bind::scheme::*;
    use guile_bind::*;

    #[test]
    fn from_into() {
        scheme::init_guile_for_current_thread();

        assert_eq!(true, SCM::from(true).into());
        assert_eq!(false, SCM::from(false).into());
    }

    #[test]
    fn bit_and() {
        scheme::init_guile_for_current_thread();

        assert_eq!(true, (SCM::from(true) & SCM::from(true)).into());
        assert_eq!(false, (SCM::from(false) & SCM::from(true)).into());
        assert_eq!(false, (SCM::from(true) & SCM::from(false)).into());
        assert_eq!(false, (SCM::from(false) & SCM::from(false)).into());
    }

    #[test]
    fn bit_or() {
        scheme::init_guile_for_current_thread();

        assert_eq!(true, (SCM::from(true) | SCM::from(true)).into());
        assert_eq!(true, (SCM::from(false) | SCM::from(true)).into());
        assert_eq!(true, (SCM::from(true) | SCM::from(false)).into());
        assert_eq!(false, (SCM::from(false) | SCM::from(false)).into());
    }

    #[test]
    fn bit_xor() {
        scheme::init_guile_for_current_thread();

        assert_eq!(false, (SCM::from(true) ^ SCM::from(true)).into());
        assert_eq!(true, (SCM::from(false) ^ SCM::from(true)).into());
        assert_eq!(true, (SCM::from(true) ^ SCM::from(false)).into());
        assert_eq!(false, (SCM::from(false) ^ SCM::from(false)).into());
    }

    #[test]
    fn default() {
        scheme::init_guile_for_current_thread();

        assert_eq!(bool::default(), SCM::<bool>::default().into());
    }

    #[test]
    fn is() {
        scheme::init_guile_for_current_thread();

        assert!(scheme::eval_string::<Any>("#t").is_bool());
        assert!(!scheme::eval_string::<Any>("'()").is_bool())
    }

    #[test]
    fn not() {
        scheme::init_guile_for_current_thread();

        assert_eq!(true, (!SCM::from(false)).into());
        assert_eq!(false, (!SCM::from(true)).into());
    }

}
