#![allow(unused_imports)]
#![allow(unused_macros)]

extern crate guile_bind;

#[cfg(test)]
mod scm_string {
    use guile_bind::features::*;
    use guile_bind::scheme::{Any, SCM};
    use guile_bind::string::*;
    use guile_bind::*;

    #[test]
    fn from_into() {
        scheme::init_guile_for_current_thread();

        assert_eq!("Hello World!",
                   String::from(SCM::<String>::from("Hello World!")));
        assert_eq!("Not Again...",
                   String::from(SCM::<String>::from("Not Again...".to_string())));
    }

    #[test]
    fn len() {
        scheme::init_guile_for_current_thread();

        let a = "Dream A Little Dream Of Me";
        assert_eq!(a.len(), SCM::<String>::from(a).len());

        let b = "";
        assert_eq!(b.len(), SCM::<String>::from(b).len());

        let c = "New Line \n not a problem";
        assert_eq!(c.len(), SCM::<String>::from(c).len());
    }

    #[test]
    fn is() {
        scheme::init_guile_for_current_thread();

        assert!(scheme::eval_string::<String>("\"asdf\"").is_string());
        assert!(!scheme::eval_string::<Any>("'(1 2 3 4)").is_string());
    }

    #[test]
    fn default() {
        scheme::init_guile_for_current_thread();

        assert_eq!(String::default(), String::from(SCM::<String>::default()));
    }
}
