#![allow(unused_imports)]
#![allow(unused_macros)]

extern crate guile_bind;

#[cfg(test)]
mod scm_list {
    use guile_bind::features::*;
    use guile_bind::list::*;
    use guile_bind::scheme::{Any, SCM};
    use guile_bind::*;

    struct Tlist<'a> {
        rust:  Vec<&'a str>,
        guile: SCM<List<String>>,
    }

    impl<'a> Tlist<'a> {
        fn new() -> Tlist<'a> {
            let rust = vec!["Agent Carter",
                            "M",
                            "Phantom",
                            "Nosferatu",
                            "A Trip to the Moon",
                            "Spione",
                            "Modern Times"];

            let guile = SCM::list(rust.iter().cloned().map(SCM::<String>::from));

            Tlist { rust:  rust,
                guile: guile, }
        }
    }

    #[test]
    fn is() {
        scheme::init_guile_for_current_thread();

        assert!(scheme::eval_string::<Any>("'()").is_list());
        assert!(scheme::eval_string::<Any>("'(1 (2 . 3) 4)").is_list());
        assert!(!scheme::eval_string::<Any>("\"asdf\"").is_list());
    }

    #[test]
    fn len() {
        scheme::init_guile_for_current_thread();

        assert_eq!(0, scheme::eval_string::<List<Any>>("'()").len());
        assert_ne!(5, scheme::eval_string::<List<Any>>("'(1 2 3 4)").len());
    }

    #[test]
    fn list() {
        scheme::init_guile_for_current_thread();
        let l = Tlist::new();

        assert_eq!(l.rust.len(), l.guile.len());

        for e in l.rust.into_iter().enumerate() {
            assert_eq!(e.1, String::from(l.guile.get(e.0.into())));
        }
    }

    #[test]
    fn car() {
        scheme::init_guile_for_current_thread();
        let l = Tlist::new();

        assert_eq!(l.rust[0], String::from(l.guile.car().unwrap()));
    }

    #[test]
    fn cdr() {
        scheme::init_guile_for_current_thread();
        let l = Tlist::new();

        l.guile.cdr()
         .expect("expected tail of list")
         .iter()
         .out_of_scm()
         .map(|v| v.unwrap())
         .zip(l.rust.iter().skip(1))
         .for_each(|(g, &r)| assert_eq!(g, r));
    }

    #[test]
    fn append() {
        scheme::init_guile_for_current_thread();
        let mut l = Tlist::new();
        let s = "Blade Runner";

        l.guile = l.guile.append(&[s.into()]);
        l.rust.append(&mut vec![s]);

        for (g, r) in l.guile.iter().zip(l.rust.into_iter()) {
            assert_eq!(String::from(g), r);
        }
    }

    #[test]
    fn xappend() {
        unimplemented!();
    }

    #[test]
    fn reverse() {
        scheme::init_guile_for_current_thread();
        let mut l = Tlist::new();

        l.guile = l.guile.reverse();

        for e in l.rust.into_iter().rev().enumerate() {
            assert_eq!(e.1, String::from(l.guile.get(e.0.into())));
        }
    }

    #[test]
    fn xreverse() {
        unimplemented!();
    }

    #[test]
    fn xset() {
        scheme::init_guile_for_current_thread();
        let mut l = Tlist::new();
        let r = "The Last Laugh";

        l.rust[3] = r;
        l.guile.xset(3.into(), &SCM::<String>::from(r));

        for e in l.rust.into_iter().enumerate() {
            assert_eq!(e.1, String::from(l.guile.get(e.0.into())));
        }
    }

    #[test]
    fn delq() {
        unimplemented!();
    }

    #[test]
    fn xdelq() {
        unimplemented!();
    }

    #[test]
    fn delv() {
        unimplemented!();
    }

    #[test]
    fn xdelv() {
        unimplemented!();
    }

    #[test]
    fn delete() {
        unimplemented!();
    }

    #[test]
    fn xdelete() {
        unimplemented!();
    }

    #[test]
    fn xdelq1() {
        unimplemented!();
    }

    #[test]
    fn xdelv1() {
        unimplemented!();
    }

    #[test]
    fn xdelete1() {
        unimplemented!();
    }

    #[test]
    fn iter() {
        scheme::init_guile_for_current_thread();
        let l = Tlist::new();

        l.guile.iter()
         .enumerate()
         .for_each(|(e, v)| assert_eq!(l.rust[e], String::from(v)));

        assert_eq!(l.rust.len(), l.guile.iter().count());
    }

    #[test]
    fn double_ended_iter() {
        scheme::init_guile_for_current_thread();
        let l = Tlist::new();

        l.guile.iter()
         .enumerate()
         .rev()
         .for_each(|(e, v)| assert_eq!(l.rust[e], String::from(v)));

        assert_eq!(l.rust.len(), l.guile.iter().rev().count());
    }

    #[test]
    fn change_into_iter() {
        scheme::init_guile_for_current_thread();
        let l = Tlist::new();

        l.rust.iter()
         .cloned()
         .into_scm::<String>()
         .map(String::from)
         .zip(l.rust.iter())
         .for_each(|(s, &r)| assert_eq!(s, r));
    }

    #[test]
    fn change_out_of_scm() {
        scheme::init_guile_for_current_thread();
        let l = Tlist::new();

        l.guile.iter()
         .out_of_scm()
         .zip(l.rust.iter())
         .for_each(|(s, &r)| assert_eq!(s.unwrap(), r));
    }
}
