#![allow(unused_imports)]
#![allow(unused_macros)]

extern crate guile_bind;

extern crate num_rational;
extern crate rand;

#[cfg(test)]
mod scm_any_number {
    use std::error::Error;

    use guile_bind::features::*;
    use guile_bind::scheme::SCM;
    use guile_bind::string::*;
    use guile_bind::*;

    use num_rational::Ratio;
    use num_rational::Rational;

    use rand::Rng;

    /*$(
                //todo sort out better ways to gen function tests
                #[test]
                fn concat_idents!($t, _from_into)() {
                    scheme::init_guile_for_current_thread();

                    for _i in  0..10 {
                        let val = ::rand::random::<$t>();

                        assert_eq!(val, SCM::try_from(val).unwrap().try_into().unwrap());
                        assert_ne!(val ^ ($t::max_value()),
                                   $t::try_from(SCM::try_from(val).unwrap()).unwrap());
                    }
                }

                #[test]
                fn [$t _add]() {
                    scheme::init_guile_for_current_thread();
                    let mut rd = ::rand::thread_rng();

                    for _i in 0..10 {
                        let v1 = rd.gen_range($t::min_value()/2, $t::max_value()/2);
                        let v2 = rd.gen_range($t::min_value()/2, $t::max_value()/2);

                        assert_eq!(v1 + v2, (SCM::try_from(v1).unwrap() +
                                             SCM::try_from(v2).unwrap()).try_into().unwrap());
                    }
                }

                #[test]
                fn [$t _sub]() {
                    scheme::init_guile_for_current_thread();
                    let mut rd = ::rand::thread_rng();

                    for _i in 0..10 {
                        let v1 = rd.gen_range($t::min_value()/2, $t::max_value()/2);
                        let v2 = rd.gen_range($t::min_value()/2, $t::max_value()/2);

                        let a1 = ::std::cmp::max(v1, v2);
                        let a2 = ::std::cmp::min(v1, v2);

                        assert_eq!(a1 - a2, (SCM::try_from(a1).unwrap() -
                                             SCM::try_from(a2).unwrap()).try_into().unwrap());
                    }
                }

                /*fn [scm_ $t _mul](scm_it) {
                    let mut rd = ::rand::thread_rng();
                    let uv = f64::sqrt(($t::max_value() - 1) as f64);
                    let lv = (if $t::min_value() == 0 {1.0} else {-1.0}) *
                        f64::sqrt(f64::abs($t::min_value() as f64));

                    for _i in 0..10 {
                        let v1 = rd.gen_range(uv as $t, lv as $t);
                        let v2 = rd.gen_range(uv as $t, lv as $t);

                        assert_eq!(v1 * v2, (SCM::from(v1) *
                                             SCM::from(v2)).into());
                    }
                }

                fn [scm_ $t _div](scm_it) {
                    let mut rd = ::rand::thread_rng();

                    for _i in 0..10 {
                        let v1 = rd.gen_range($t::min_value()/2, $t::max_value()/2);
                        let v2 = rd.gen_range($t::min_value()/2, $t::max_value()/2);

                        let a1 = ::std::cmp::max(v1, v2);
                        let a2 = ::std::cmp::min(v1, v2);

                        assert_eq!(a1 - a2, (SCM::from(a1) -
                                             SCM::from(a2)).into());
                    }
                }*/

            )**/

    #[test]
    fn rational_from_into() {
        scheme::init_guile_for_current_thread();
        let v = Rational::new(53, 5);

        assert_eq!(v, SCM::from(v).try_into().unwrap());
        assert_ne!(v, SCM::from(Rational::new(52, 5)).try_into().unwrap())
    }

    #[test]
    fn rational_numer_denom() {
        scheme::init_guile_for_current_thread();
        let v = Rational::new(23, 7);
        let s = SCM::from(v);

        assert_eq!(*v.numer(), isize::try_from(s.numerator()).unwrap());
        assert_eq!(*v.denom(), isize::try_from(s.denominator()).unwrap());
    }

    #[test]
    fn error_sizing() {
        scheme::init_guile_for_current_thread();
        let h = SCM::from(i32::max_value() / 2);

        match i8::try_from(h) {
            Err(e) => assert_eq!("out-of-range", e.scmerr.errcode),
            _ => panic!("scm => i8 should error 'out-of-range'"),
        }
    }

    #[test]
    fn to_scm_string() {
        scheme::init_guile_for_current_thread();
        let v = SCM::from(54).to_scm_string(10);

        assert_eq!(2, v.len());
        assert_eq!("54", String::try_from(v).unwrap());
    }

    #[test]
    fn neg() {
        scheme::init_guile_for_current_thread();
        let v = 26;

        assert_eq!(-v, i32::try_from(-SCM::from(v)).unwrap());
    }

    #[test]
    fn bitand() {
        scheme::init_guile_for_current_thread();
        let a = 74;
        let b = 33;

        assert_eq!(a & b, i32::try_from(SCM::from(a) & SCM::from(b)).unwrap());
    }

    #[test]
    fn bitor() {
        scheme::init_guile_for_current_thread();
        let a = 74;
        let b = 33;

        assert_eq!(a | b, i32::try_from(SCM::from(a) | SCM::from(b)).unwrap());
    }

    #[test]
    fn bitxord() {
        scheme::init_guile_for_current_thread();
        let a = 74;
        let b = 33;

        assert_eq!(a ^ b, i32::try_from(SCM::from(a) ^ SCM::from(b)).unwrap());
    }

    #[test]
    fn shl() {
        scheme::init_guile_for_current_thread();
        let a = 74;
        let b = 3;

        assert_eq!(a << b, i32::try_from(SCM::from(a) << SCM::from(b)).unwrap());
    }

    #[test]
    fn shr() {
        scheme::init_guile_for_current_thread();
        let a = 74;
        let b = 3;

        assert_eq!(a >> b, i32::try_from(SCM::from(a) >> SCM::from(b)).unwrap());
    }

    #[test]
    fn is_odd() {
        scheme::init_guile_for_current_thread();

        assert!(bool::from(SCM::from(853).is_odd()));
        assert!(!bool::from(SCM::from(332).is_odd()));
    }

    #[test]
    fn is_even() {
        scheme::init_guile_for_current_thread();

        assert!(bool::from(SCM::from(926).is_even()));
        assert!(!bool::from(SCM::from(167).is_even()));
    }
}

//gen_tests_for_common_numbers!(i8, i16, i32, i64, isize, u8, u16, u32, u64, usize);
