#![allow(unused_imports)]
#![allow(unused_macros)]

extern crate guile_bind;

#[cfg(test)]
mod scm_symbol {
    use guile_bind::features::*;
    use guile_bind::keyword::*;
    use guile_bind::scheme::{Any, SCM};
    use guile_bind::symbol::*;
    use guile_bind::*;

    #[test]
    fn from_into() {
        scheme::init_guile_for_current_thread();

        assert!(SCM::<Symbol>::from(SCM::<String>::from("sos")).is_symbol());
        assert!(SCM::<String>::from(scheme::eval_string::<Symbol>("'cda")).is_string());
        assert!(SCM::<Symbol>::from(scheme::eval_string::<Keyword>("#:keyword")).is_symbol());
        assert_eq!("Fragile",
                   String::from(SCM::<String>::from(SCM::<Symbol>::from("Fragile".to_string()))));
    }

    #[test]
    fn is() {
        scheme::init_guile_for_current_thread();

        assert!(scheme::eval_string::<Symbol>("'abc").is_symbol());
        assert!(scheme::eval_string::<Any>("'def").is_symbol());
        assert!(!scheme::eval_string::<Symbol>("'(1 2 3)").is_symbol());
    }

    #[test]
    fn default() {
        scheme::init_guile_for_current_thread();

        assert_eq!("",
                   String::from(SCM::<String>::from(SCM::<Symbol>::default())));
    }

    #[test]
    fn gensym() {
        scheme::init_guile_for_current_thread();

        let pf: SCM<String> = "prefix".into();
        let sybl: String = SCM::<String>::from(SCM::<Symbol>::gensym(&pf)).into();

        assert!(sybl.starts_with("prefix"));
    }
}
