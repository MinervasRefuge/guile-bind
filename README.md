# guile_bind
 
project abandoned because of 'longjmp' memory unsafety concerns with no viable workaround to my knowledge..

---

*guile_bind* is a project to combine the best of both worlds; [Rust](https://www.rust-lang.org) (c-like) a system level programming language and [Guile](https://www.gnu.org/software/guile/) (Scheme, LISP) a charming scriptable language; into a Rust API.

Currently not much is supported in the way of concrete examples and function code. Most of the focus now is laying the groundwork and providing a solid foundation in which to lay all of Guiles primitive types on.

### Testing
If you wish to test out the tests currently available, run as `cargo test -- --test-threads=1` This is a single threaded api. A fact which has bitten me more then once given each test (or test groups) recieves its own thread.

### Optional Features
| Feature          | Compiler Channel | Description                                                            |
|------------------|------------------|------------------------------------------------------------------------|
| _all_            | nightly          | Enables all of the features                                            |
| _common-try_     | nightly          | Uses `std::convert::{TryFrom, TryInto}`                                |
| _better-dbg-fmt_ | nightly          | __BROKEN after 2018-02-1__ Allows `SCM<_>` debug to display type name. |
| _proc-macro_     | nightly          | Attribute for custom scm functions                                     |
