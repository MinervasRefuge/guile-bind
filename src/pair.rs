// =============================================================================
//                             §   6 . 6 . 8   P a i r
// =============================================================================

use guile;

use scheme::SCM;

// (pair? '(1 . 2)) => #t
// (pair? '(1 2)) => #t
// (cdr '(1 . 2)) => 2
// (cdr '(1 2)) => (2)
// special cases need to be consided carefully for type safety
// guess thats what you get for type safeing a ducktyping lang

impl<T: All> SCM<T> {
    pub fn cons<Q: All>(v1: &SCM<Q>, v2: &SCM<Q>) -> SCM<List<Q>> {
        SCM::new(unsafe {guile::scm_cons(v1.gptr, v2.gptr)})
    }

    pub fn is_pair_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_pair_p(self.gptr) }.into()
    }

    pub fn is_pair(&self) -> bool {
        self.is_pair_scm().into()
    }
}
