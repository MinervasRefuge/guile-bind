/*                 WARNING                 */
/* Don't edit this file as it is generated */
/*        all changes will be lost         */
//source: src/number_operators.rs.m4

use guile;
use number::*;
use scheme::SCM;

use std::ops::{Add, Div, Mul, Sub};

// =============================================================================
//                                 A d d   O p
// =============================================================================

impl Add<SCM<Integer>> for SCM<Integer> {
    type Output = SCM<Integer>;

    fn add(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Rational>> for SCM<Rational> {
    type Output = SCM<Rational>;

    fn add(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Real>> for SCM<Real> {
    type Output = SCM<Real>;

    fn add(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Complex>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn add(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Rational>> for SCM<Integer> {
    type Output = SCM<Rational>;

    fn add(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Real>> for SCM<Integer> {
    type Output = SCM<Real>;

    fn add(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Complex>> for SCM<Integer> {
    type Output = SCM<Complex>;

    fn add(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Integer>> for SCM<Rational> {
    type Output = SCM<Rational>;

    fn add(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Real>> for SCM<Rational> {
    type Output = SCM<Real>;

    fn add(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Complex>> for SCM<Rational> {
    type Output = SCM<Complex>;

    fn add(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Integer>> for SCM<Real> {
    type Output = SCM<Real>;

    fn add(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Rational>> for SCM<Real> {
    type Output = SCM<Real>;

    fn add(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Complex>> for SCM<Real> {
    type Output = SCM<Complex>;

    fn add(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Integer>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn add(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Rational>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn add(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

impl Add<SCM<Real>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn add(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}

// =============================================================================
//                                 S u b   O p
// =============================================================================

impl Sub<SCM<Integer>> for SCM<Integer> {
    type Output = SCM<Integer>;

    fn sub(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Rational>> for SCM<Rational> {
    type Output = SCM<Rational>;

    fn sub(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Real>> for SCM<Real> {
    type Output = SCM<Real>;

    fn sub(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Complex>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn sub(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Rational>> for SCM<Integer> {
    type Output = SCM<Rational>;

    fn sub(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Real>> for SCM<Integer> {
    type Output = SCM<Real>;

    fn sub(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Complex>> for SCM<Integer> {
    type Output = SCM<Complex>;

    fn sub(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Integer>> for SCM<Rational> {
    type Output = SCM<Rational>;

    fn sub(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Real>> for SCM<Rational> {
    type Output = SCM<Real>;

    fn sub(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Complex>> for SCM<Rational> {
    type Output = SCM<Complex>;

    fn sub(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Integer>> for SCM<Real> {
    type Output = SCM<Real>;

    fn sub(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Rational>> for SCM<Real> {
    type Output = SCM<Real>;

    fn sub(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Complex>> for SCM<Real> {
    type Output = SCM<Complex>;

    fn sub(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Integer>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn sub(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Rational>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn sub(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

impl Sub<SCM<Real>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn sub(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}

// =============================================================================
//                                 M u l   O p
// =============================================================================

impl Mul<SCM<Integer>> for SCM<Integer> {
    type Output = SCM<Integer>;

    fn mul(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Rational>> for SCM<Rational> {
    type Output = SCM<Rational>;

    fn mul(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Real>> for SCM<Real> {
    type Output = SCM<Real>;

    fn mul(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Complex>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn mul(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Rational>> for SCM<Integer> {
    type Output = SCM<Rational>;

    fn mul(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Real>> for SCM<Integer> {
    type Output = SCM<Real>;

    fn mul(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Complex>> for SCM<Integer> {
    type Output = SCM<Complex>;

    fn mul(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Integer>> for SCM<Rational> {
    type Output = SCM<Rational>;

    fn mul(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Real>> for SCM<Rational> {
    type Output = SCM<Real>;

    fn mul(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Complex>> for SCM<Rational> {
    type Output = SCM<Complex>;

    fn mul(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Integer>> for SCM<Real> {
    type Output = SCM<Real>;

    fn mul(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Rational>> for SCM<Real> {
    type Output = SCM<Real>;

    fn mul(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Complex>> for SCM<Real> {
    type Output = SCM<Complex>;

    fn mul(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Integer>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn mul(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Rational>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn mul(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

impl Mul<SCM<Real>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn mul(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}

// =============================================================================
//                                 D i v   O p
// =============================================================================

impl Div<SCM<Integer>> for SCM<Integer> {
    type Output = SCM<Rational>;

    fn div(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Rational>> for SCM<Rational> {
    type Output = SCM<Rational>;

    fn div(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Real>> for SCM<Real> {
    type Output = SCM<Real>;

    fn div(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Complex>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn div(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Rational>> for SCM<Integer> {
    type Output = SCM<Rational>;

    fn div(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Real>> for SCM<Integer> {
    type Output = SCM<Real>;

    fn div(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Complex>> for SCM<Integer> {
    type Output = SCM<Complex>;

    fn div(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Integer>> for SCM<Rational> {
    type Output = SCM<Rational>;

    fn div(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Real>> for SCM<Rational> {
    type Output = SCM<Real>;

    fn div(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Complex>> for SCM<Rational> {
    type Output = SCM<Complex>;

    fn div(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Integer>> for SCM<Real> {
    type Output = SCM<Real>;

    fn div(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Rational>> for SCM<Real> {
    type Output = SCM<Real>;

    fn div(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Complex>> for SCM<Real> {
    type Output = SCM<Complex>;

    fn div(self, rhs: SCM<Complex>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Integer>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn div(self, rhs: SCM<Integer>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Rational>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn div(self, rhs: SCM<Rational>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}

impl Div<SCM<Real>> for SCM<Complex> {
    type Output = SCM<Complex>;

    fn div(self, rhs: SCM<Real>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}
