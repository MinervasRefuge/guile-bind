use guile;
use scheme::*;

pub struct Port {}

impl All for Port {}

impl SCM<Port> {
    //§6.14.10.3  String Ports

    pub fn open_output_string() -> SCM<Port> {
        unsafe { guile::scm_open_output_string() }.into()
    }

    pub fn get_output_string(&mut self) -> SCM<String> {
        unsafe { guile::scm_get_output_string(self.gptr) }.into()
    }

    pub fn close_port(self) {
        unsafe { guile::scm_close_port(self.gptr) };
    }
}

