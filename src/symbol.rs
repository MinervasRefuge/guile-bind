// =============================================================================
//                          §   6 . 6 . 6   S y m b o l
// =============================================================================

use features::*;
use guile;
use keyword::*;
use list::*;
use scheme::*;

use std::default::Default;
use std::marker::PhantomData;
use std::*;

pub struct Symbol {}

impl All for Symbol {}

impl From<SCM<Keyword>> for SCM<Symbol> {
    fn from(v: SCM<Keyword>) -> Self {
        unsafe { guile::scm_keyword_to_symbol(v.gptr) }.into()
    }
}

impl From<SCM<String>> for SCM<Symbol> {
    fn from(v: SCM<String>) -> Self {
        unsafe { guile::scm_string_to_symbol(v.gptr) }.into()
    }
}

impl From<String> for SCM<Symbol> {
    fn from(v: String) -> SCM<Symbol> {
        SCM::<Symbol>::from(v.as_str())
    }
}

impl<'a> From<&'a str> for SCM<Symbol> {
    fn from(v: &'a str) -> Self {
        unsafe { guile::scm_from_locale_symboln(write_str_to_malloc_mem(v), v.len()) }.into()
    }
}

impl SCM<Symbol> {
    pub fn gensym(prefix: &SCM<String>) -> SCM<Symbol> {
        unsafe { guile::scm_gensym(prefix.gptr) }.into()
    }
}

impl Default for SCM<Symbol> {
    fn default() -> Self {
        SCM::<String>::from("").into()
    }
}

impl<T: All> SCM<T> {
    pub fn is_symbol_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_symbol_p(self.gptr) }.into()
    }

    pub fn is_symbol(&self) -> bool {
        self.is_symbol_scm().into()
    }
}

impl<T: All> TryAs<Symbol> for SCM<T> {
    fn try_as(&self) -> Option<SCM<Symbol>> {
        if self.is_symbol() {
            Some(self.gptr.into())
        } else {
            None
        }
    }
}
