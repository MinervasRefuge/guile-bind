// =============================================================================
//          §   6 . 6 . 2   N u m e r i c a l   D a t a   T y p e s
// =============================================================================

use num::Float;
use num::Num;

use num_rational::Ratio;
pub use num_rational::Rational;

use num_complex;
use num_complex::Complex64;

use error::{error_match, ESCM};
use features::*;
use guile;
use scheme;
use scheme::{All, TryAs, SCM};
use symbol::*;

use std::cell::Cell;
use std::clone::Clone;
use std::cmp::{Ordering, PartialEq, PartialOrd};
use std::error::Error;
use std::fmt::Display;
use std::fmt::Formatter;
use std::marker::PhantomData;
use std::ops::{BitAnd, BitOr, BitXor, Mul, Neg, Not, Rem, Shl, Shr};
use std::*;

// =============================================================================
//           N u m e r i c a l   T o w e r   M a r k e r   T y p e s
// =============================================================================
/// things that must be said
pub struct Integer {}
//rationals => num_rational::Rational
pub struct Real {}
pub type Complex = Complex64;

impl All for Integer {}
impl All for Rational {}
impl All for Real {}
impl All for Complex {}

// =============================================================================
//                          N u m e r i c a l   T o w e r
// =============================================================================

pub trait NTInteger {}
pub trait NTRational {}
pub trait NTReal {}
pub trait NTComplex {}

macro_rules! impl_for_number {
    ($($t:ty => $($tt:ty)|*),*) => {
        $(
            $(
                impl $tt for $t {}
            )*
        )*
    }
}

impl_for_number!(Integer  => NTInteger | NTRational | NTReal | NTComplex,
                 Rational => NTRational | NTReal | NTComplex,
                 Real     => NTReal | NTComplex,
                 Complex  => NTComplex);

impl All for NTInteger {}
impl All for NTRational {}
impl All for NTReal {}
impl All for NTComplex {}

// =============================================================================
//                        E r r o r   H a n d e l i n g
// =============================================================================

#[derive(Debug, Clone)]
pub struct NumberIncompatibilityError {
    pub scmerr: &'static ESCM,
}

impl error::Error for NumberIncompatibilityError {
    fn description(&self) -> &str {
        self.scmerr.desc
    }
}

impl Display for NumberIncompatibilityError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

// =============================================================================
//                       S C M   I n t e g e r   T y p e
// =============================================================================

macro_rules! gen_type_to_fn {
    ($($t:ty => $to:path | $from:path),*) => {
        $(
            impl From<$t> for SCM<Integer> {
                fn from(v: $t) -> Self {
                    unsafe{ $from(v) }.into()
                }
            }

            impl<'a> TryFrom<SCM<Integer>> for $t {
                type Error = NumberIncompatibilityError;

                fn try_from(v: SCM<Integer>) -> Result<Self, Self::Error> {
                    unsafe {
                        let mut result = Cell::new(None);

                        scheme::internal_catch(SCM::from(true), &|| {
                            result.set(Some(Ok($to(v.gptr))));
                        }, &|key, args| {
                            let rkey = String::from(key);

                            result.set(Some(Err(NumberIncompatibilityError {
                                scmerr: error_match(rkey.as_str()).expect("Unknown Error")
                            })));
                        });

                        result.get_mut().clone().unwrap()
                    }
                }
            }
        )*
    };
    ($pw:tt, $($t:ty => $ct:ty | $to:path | $from:path),*) => {
        $(
            #[cfg(target_pointer_width = $pw)]
            impl From<$t> for SCM<Integer> {
                fn from(v: $t) -> Self {
                    unsafe { $from(v as $ct) }.into()
                }
            }

            #[cfg(target_pointer_width = $pw)]
            impl TryFrom<SCM<Integer>> for $t {
                type Error = NumberIncompatibilityError;

                fn try_from(v: SCM<Integer>) -> Result<Self, Self::Error> {
                    unsafe {
                        let mut result = Cell::new(None);

                        scheme::internal_catch(SCM::from(true), &|| {
                            result.set(Some(Ok($to(v.gptr))));
                        }, &|key, args| {
                            let rkey = String::from(key);

                            result.set(Some(Err(NumberIncompatibilityError {
                                scmerr: error_match(rkey.as_str()).expect("Unknown Error")
                            })));
                        });

                        result.get_mut().clone().unwrap().map(|v| v as $t)
                    }
                }
            }
        )*
    }
}

gen_type_to_fn!(
    i8 => guile::scm_to_int8    | guile::scm_from_int8,
    u8 => guile::scm_to_uint8   | guile::scm_from_uint8,
    i16 => guile::scm_to_int16  | guile::scm_from_int16,
    u16 => guile::scm_to_uint16 | guile::scm_from_uint16,
    i32 => guile::scm_to_int32  | guile::scm_from_int32,
    u32 => guile::scm_to_uint32 | guile::scm_from_uint32,
    i64 => guile::scm_to_int64  | guile::scm_from_int64,
    u64 => guile::scm_to_uint64 | guile::scm_from_uint64
);

gen_type_to_fn!("64",
                isize => i64 | guile::scm_to_int64 | guile::scm_from_int64,
                usize => u64 | guile::scm_to_uint64 | guile::scm_from_uint64);

gen_type_to_fn!("32",
                isize => i32 | guile::scm_to_int32 | guile::scm_from_int32,
                usize => u32 | guile::scm_to_uint32 | guile::scm_from_uint32);

impl Rem for SCM<Integer> {
    type Output = Self;

    fn rem(self, rhs: Self) -> Self {
        unsafe { guile::scm_remainder(self.gptr, rhs.gptr) }.into()
    }
}

impl BitAnd for SCM<Integer> {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self {
        unsafe { guile::scm_logand(self.gptr, rhs.gptr) }.into()
    }
}

impl BitOr for SCM<Integer> {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self {
        unsafe { guile::scm_logior(self.gptr, rhs.gptr) }.into()
    }
}

impl BitXor for SCM<Integer> {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self {
        unsafe { guile::scm_logxor(self.gptr, rhs.gptr) }.into()
    }
}

impl Not for SCM<Integer> {
    type Output = Self;

    fn not(self) -> Self {
        unsafe { guile::scm_lognot(self.gptr) }.into()
    }
}

impl Shl<Self> for SCM<Integer> {
    type Output = Self;

    fn shl(self, rhs: Self) -> Self {
        unsafe { guile::scm_ash(self.gptr, rhs.gptr) }.into()
    }
}

impl Shr<Self> for SCM<Integer> {
    type Output = Self;

    fn shr(self, rhs: Self) -> Self {
        unsafe { guile::scm_ash(self.gptr, (-rhs).gptr) }.into()
    }
}

impl SCM<Integer> {
    pub fn is_odd(&self) -> SCM<bool> {
        unsafe { guile::scm_odd_p(self.gptr) }.into()
    }

    pub fn is_even(&self) -> SCM<bool> {
        unsafe { guile::scm_even_p(self.gptr) }.into()
    }
}

impl<T: All> SCM<T> {
    pub fn is_integer_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_number_p(self.gptr) }.into()
    }

    pub fn is_integer(&self) -> bool {
        (unsafe { guile::scm_is_integer(self.gptr) } != 0)
    }
}

impl<T: All> TryAs<Integer> for SCM<T> {
    fn try_as(&self) -> Option<SCM<Integer>> {
        if self.is_integer() {
            Some(self.gptr.into())
        } else {
            None
        }
    }
}

// =============================================================================
//                        S C M   R a t i o n a l   T y p e
// =============================================================================

impl<T> From<Ratio<T>> for SCM<Rational>
    where T: Clone + ::num::Integer,
          SCM<Integer>: From<T>
{
    fn from(v: Ratio<T>) -> Self {
        SCM::from(v.numer().clone()) / SCM::from(v.denom().clone())
    }
}

macro_rules! impl_for_tryfrom_for_external_struct {
    ($($t:ty),*) => {
        $(
            impl TryFrom<SCM<Rational>> for Ratio<$t>{
                type Error = NumberIncompatibilityError;

                fn try_from(v: SCM<Rational>) -> Result<Ratio<$t>, Self::Error> {
                    Result::Ok(Ratio::new(v.numerator().try_into()?,
                                          v.denominator().try_into()?))
                }
            }
        )*
    }
}

impl_for_tryfrom_for_external_struct!(i8, i16, i32, i64, isize, u8, u16, u32, u64, usize);

impl<T: NTRational + All> SCM<T> {
    pub fn rationalize<Q: NTReal + All>(self, eps: SCM<Q>) -> SCM<Rational> {
        unsafe { guile::scm_rationalize(self.gptr, eps.gptr) }.into()
    }
}

impl<T: All> SCM<T> {
    pub fn is_rational_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_rational_p(self.gptr) }.into()
    }

    pub fn is_rational(&self) -> bool {
        self.is_rational_scm().into()
    }
}

impl<T: All> TryAs<Rational> for SCM<T> {
    fn try_as(&self) -> Option<SCM<Rational>> {
        if self.is_rational() {
            Some(self.gptr.into())
        } else {
            None
        }
    }
}

// =============================================================================
//                            S C M   R e a l   T y p e
// =============================================================================

impl From<f32> for SCM<Real> {
    fn from(v: f32) -> Self {
        unsafe { guile::scm_from_double(v as f64) }.into()
    }
}

impl From<f64> for SCM<Real> {
    fn from(v: f64) -> Self {
        unsafe { guile::scm_from_double(v) }.into()
    }
}

impl<T: NTReal + All> TryFrom<SCM<T>> for f64 {
    type Error = Box<::std::error::Error>;

    fn try_from(v: SCM<T>) -> Result<Self, Self::Error> {
        //todo better error handeling
        Result::Ok(unsafe { guile::scm_to_double(v.gptr) })
    }
}

impl<T: NTReal + All> SCM<T> {
    pub fn numerator(&self) -> SCM<Integer> {
        unsafe { guile::scm_numerator(self.gptr) }.into()
    }

    pub fn denominator(&self) -> SCM<Integer> {
        unsafe { guile::scm_denominator(self.gptr) }.into()
    }

    pub fn is_inf(&self) -> SCM<bool> {
        unsafe { guile::scm_inf_p(self.gptr) }.into()
    }

    pub fn is_nan(&self) -> SCM<bool> {
        unsafe { guile::scm_nan_p(self.gptr) }.into()
    }

    pub fn is_pos(&self) -> SCM<bool> {
        unsafe { guile::scm_positive_p(self.gptr) }.into()
    }

    pub fn is_neg(&self) -> SCM<bool> {
        unsafe { guile::scm_negative_p(self.gptr) }.into()
    }

    pub fn is_zero(&self) -> SCM<bool> {
        unsafe { guile::scm_zero_p(self.gptr) }.into()
    }

    pub fn to_exact(&self) -> SCM<T> {
        unsafe { guile::scm_inexact_to_exact(self.gptr) }.into()
    }

    pub fn to_inexact(&self) -> SCM<T> {
        unsafe { guile::scm_exact_to_inexact(self.gptr) }.into()
    }

    /*pub fn is_finite(&self) -> SCM<bool> {
        SCM::new(unsafe {guile::scm_finite_p(self.gptr)})
    }*/

    pub fn nan() -> SCM<Real> {
        unsafe { guile::scm_nan() }.into()
    }

    pub fn inf() -> SCM<Real> {
        unsafe { guile::scm_inf() }.into()
    }
}

impl<T: All> SCM<T> {
    pub fn is_real_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_real_p(self.gptr) }.into()
    }

    pub fn is_real(&self) -> bool {
        self.is_real_scm().into()
    }
}

impl<T: All> TryAs<Real> for SCM<T> {
    fn try_as(&self) -> Option<SCM<Real>> {
        if self.is_real() {
            Some(self.gptr.into())
        } else {
            None
        }
    }
}

// =============================================================================
//                          S C M   C o m p l e x   T y p e
// =============================================================================

impl<T> From<num_complex::Complex<T>> for SCM<Complex>
    where T: Clone + Num,
          SCM<NTReal>: From<T>
{
    fn from(v: num_complex::Complex<T>) -> Self {
        unsafe { guile::scm_make_rectangular(SCM::from(v.re).gptr, SCM::from(v.im).gptr) }.into()
    }
}

impl SCM<Complex> {
    pub fn make_rectangular<Q: NTReal + All, T: Into<SCM<Q>>>(real: T, imag: T) -> SCM<Complex> {
        unsafe { guile::scm_make_rectangular(real.into().gptr, imag.into().gptr) }.into()
    }

    pub fn make_polar<Q: NTReal + All, T: Into<SCM<Q>>>(mag: T, ang: T) -> SCM<Complex> {
        unsafe { guile::scm_make_polar(mag.into().gptr, ang.into().gptr) }.into()
    }
}

impl<T: NTComplex + All> SCM<T> {
    pub fn real_part(&self) -> SCM<Real> {
        unsafe { guile::scm_real_part(self.gptr) }.into()
    }

    pub fn imag_part(&self) -> SCM<Real> {
        unsafe { guile::scm_imag_part(self.gptr) }.into()
    }

    pub fn magnitude(&self) -> SCM<Real> {
        unsafe { guile::scm_magnitude(self.gptr) }.into()
    }

    pub fn angle(&self) -> SCM<Real> {
        unsafe { guile::scm_angle(self.gptr) }.into()
    }
}

impl<T: All> SCM<T> {
    pub fn is_complex_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_complex_p(self.gptr) }.into()
    }

    pub fn is_complex(&self) -> bool {
        self.is_complex_scm().into()
    }
}

impl<T: All> TryAs<Complex> for SCM<T> {
    fn try_as(&self) -> Option<SCM<Complex>> {
        if self.is_complex() {
            Some(self.gptr.into())
        } else {
            None
        }
    }
}

// =============================================================================
//                        S C M   N u m b e r   R u l e s
// =============================================================================

impl<T: NTComplex + All> Neg for SCM<T>
    where SCM<T>: Mul<SCM<Integer>, Output = SCM<T>>
{
    type Output = Self;

    fn neg(self) -> Self::Output {
        self * SCM::<Integer>::from(-1)
    }
}

impl<T: NTComplex + All, Q: NTComplex + All> PartialEq<SCM<Q>> for SCM<T> {
    fn eq(&self, other: &SCM<Q>) -> bool {
        SCM::from(unsafe { guile::scm_num_eq_p(self.gptr, other.gptr) }).into()
    }
}

impl<T: NTComplex + All, Q: NTComplex + All> PartialOrd<SCM<Q>> for SCM<T> {
    fn partial_cmp(&self, other: &SCM<Q>) -> Option<Ordering> {
        unsafe {
            if SCM::from(guile::scm_num_eq_p(self.gptr, other.gptr)).into() {
                return Some(Ordering::Equal);
            }

            if SCM::from(guile::scm_less_p(self.gptr, other.gptr)).into() {
                return Some(Ordering::Less);
            }

            if SCM::from(guile::scm_gr_p(self.gptr, other.gptr)).into() {
                return Some(Ordering::Greater);
            }

            None
        }
    }

    fn lt(&self, other: &SCM<Q>) -> bool {
        SCM::from(unsafe { guile::scm_less_p(self.gptr, other.gptr) }).into()
    }

    fn le(&self, other: &SCM<Q>) -> bool {
        SCM::from(unsafe { guile::scm_leq_p(self.gptr, other.gptr) }).into()
    }

    fn gt(&self, other: &SCM<Q>) -> bool {
        SCM::from(unsafe { guile::scm_gr_p(self.gptr, other.gptr) }).into()
    }

    fn ge(&self, other: &SCM<Q>) -> bool {
        SCM::from(unsafe { guile::scm_geq_p(self.gptr, other.gptr) }).into()
    }
}

impl<T: NTComplex + All> SCM<T> {
    pub fn is_exact(&self) -> SCM<bool> {
        unsafe { guile::scm_exact_p(self.gptr) }.into()
    }

    pub fn is_inexact(&self) -> SCM<bool> {
        unsafe { guile::scm_inexact_p(self.gptr) }.into()
    }
}

// =============================================================================
//                              U t i l i t i e s
// =============================================================================

impl From<SCM<char>> for SCM<Integer> {
    fn from(v: SCM<char>) -> Self {
        unsafe { guile::scm_char_to_integer(v.gptr) }.into()
    }
}

impl<T: NTReal + All> Default for SCM<T> {
    fn default() -> Self {
        SCM::<Integer>::from(0).gptr.into()
    }
}

impl Default for SCM<Complex> {
    fn default() -> Self {
        unimplemented!()
    }
}

impl<T: NTComplex + All> Display for SCM<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", String::from(self.to_scm_string(10)))
    }
}

impl<T: NTComplex + All> SCM<T> {
    pub fn to_scm_string<Q: Into<SCM<Integer>>>(&self, base: Q) -> SCM<String> {
        unsafe { guile::scm_number_to_string(self.gptr, base.into().gptr) }.into()
    }
}
