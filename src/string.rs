use guile;
use number::*;
use scheme;
use scheme::*;
use symbol::Symbol;
use symbol::*;

use libc;

use std::default::Default;
use std::marker::PhantomData;
use std::*;

impl All for String {}

impl From<SCM<String>> for String {
    fn from(v: SCM<String>) -> Self {
        unsafe {
            let mut cstr_len: libc::size_t = 0;
            let ptr_cstr = guile::scm_to_locale_stringn(v.gptr, (&mut cstr_len) as *mut _);

            let rstring =
                slice::from_raw_parts(ptr_cstr, cstr_len)
                .iter()
                .map(|&v| char::from_u32_unchecked(v as u32)) // does this need to be 'checked'?
                .collect::<String>();

            libc::free(ptr_cstr as *mut libc::c_void);

            rstring
        }
    }
}

impl From<SCM<Symbol>> for SCM<String> {
    fn from(v: SCM<Symbol>) -> Self {
        unsafe { guile::scm_symbol_to_string(v.gptr) }.into()
    }
}

impl From<SCM<Symbol>> for String {
    fn from(v: SCM<Symbol>) -> Self {
        SCM::<String>::from(v).into()
    }
}

impl From<String> for SCM<String> {
    fn from(v: String) -> Self {
        SCM::<String>::from(v.as_str())
    }
}

impl<'a> From<&'a str> for SCM<String> {
    fn from(v: &'a str) -> Self {
        unsafe {
            guile::scm_take_locale_stringn(
                scheme::write_str_to_malloc_mem(v),
                v.len())
        }.into()
    }
}

impl Default for SCM<String> {
    fn default() -> Self {
        String::default().into()
    }
}

impl SCM<String> {
    pub fn len(&self) -> usize {
        unsafe { guile::scm_c_string_length(self.gptr) }
    }
}

impl<T: All> SCM<T> {
    pub fn is_string_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_string_p(self.gptr) }.into()
    }

    pub fn is_string(&self) -> bool {
        (unsafe { guile::scm_is_string(self.gptr) } != 0)
    }
}

impl<T: All> TryAs<String> for SCM<T> {
    fn try_as(&self) -> Option<SCM<String>> {
        if self.is_string() {
            Some(self.gptr.into())
        } else {
            None
        }
    }
}
