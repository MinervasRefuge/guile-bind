use guile;
use scheme::{write_str_to_managed_mem, All, SCM};

use std::os::raw::c_void;
use std::*;

pub struct SFn {}
impl All for SFn {}

pub fn register_scm_fn1(name: &str, f: unsafe extern "C" fn(guile::SCM) -> guile::SCM) -> SCM<SFn> {
    unsafe {
        guile::scm_c_define_gsubr(write_str_to_managed_mem(name),
                                  1,
                                  0,
                                  0,
                                  mem::transmute(f as *const unsafe extern "C" fn(guile::SCM)
                                                  -> guile::SCM))
    }.into()
}

pub fn register_scm_fn2(name: &str,
                        f: unsafe extern "C" fn(guile::SCM, guile::SCM) -> guile::SCM)
                        -> SCM<SFn>
{
    unsafe {
        guile::scm_c_define_gsubr(write_str_to_managed_mem(name),
                                  2,
                                  0,
                                  0,
                                  mem::transmute(f as *const unsafe extern "C" fn(guile::SCM,
                                                  guile::SCM)
                                                  -> guile::SCM))
    }.into()
}

//todo fix type args to allow optional `mut`
#[macro_export]
macro_rules! scmFn {
    (fn $scmName:ident | $name:ident ( $($v:ident : SCM< $t:ty >),* )
                                       -> SCM<$ft:ty> $body:block) => {
        #[allow(dead_code, unused_mut)]
        fn $name($(mut $v: SCM<$t>),*) -> SCM<$ft> $body

        #[allow(unused_mut)]
        unsafe extern "C" fn $scmName($(mut $v: InternalSCM),*) -> InternalSCM {
            $(
                let mut $v = SCM::<Any>::from($v).try_scm::<$t>();
                if $v.is_none() { return SCM::from(false).gptr; }
                let mut $v = $v.unwrap();
            )*

            let res: SCM<$ft> = $body;
            res.gptr
        }
    }
}
