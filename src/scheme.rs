use features::*;
use guile;
use number::*;
use port::*;
use registrar::SFn;
use symbol::*;

use libc;

use std::cell::Cell;
use std::clone::Clone;
use std::fmt::{Debug, Display, Error, Formatter};
use std::marker::PhantomData;
use std::sync::Mutex;
use std::*;

/*impl Guile {
    pub fn with_guile(mut uf: &FnMut(&mut LiveGuile) -> ()) {
        
        //let mut ufref: &mut FnMut() -> () = &mut uf;
        
        unsafe extern fn scm_local_handle(d: *mut os::raw::c_void) -> *mut os::raw::c_void {

            //todo Should not do this type trickery >.>
            let cd: &mut &mut FnMut(&LiveGuile) -> () = mem::transmute(d);
        
            cd(&mut LiveGuile::new());

            ptr::null_mut()
        }
        
        unsafe {
            cbin::scm_with_guile(Some(scm_local_handle),
                                 mem::transmute::<&mut &FnMut(&mut LiveGuile) ->(), *mut os::raw::c_void>(&mut uf));
        }
    }
}*/

lazy_static! {
    static ref GUILE_SYNC_INIT: sync::Mutex<usize> = sync::Mutex::new(0);
    static ref FN_DISPLAY: SCM<SFn> = {
        init_guile_for_current_thread();
        eval_string::<SFn>("display").unwrap()
    };
}

pub fn init_guile_for_current_thread() {
    //guile is comunicating should be done one at a time
    //multiple threads in the test unit cause issues. can be fixed with --test-threads=1
    //but this works as well ^_^
    let _lock = GUILE_SYNC_INIT.lock().unwrap();

    unsafe { guile::scm_init_guile() };
}

//using scm_c_continuation_barrier prevents total system exit. Will loose error though..
pub fn eval_string<T: All>(s: &str) -> Result<SCM<T>, ()> {
    let cs = ffi::CString::new(s).unwrap();

    let eval = unsafe { guile::scm_c_with_continuation_barrier(
        Some(mem::transmute(guile::scm_c_eval_string as *mut fn(*const os::raw::c_char) -> guile::SCM)),
        mem::transmute(cs.as_ptr()))};

    if eval == ptr::null_mut() {
        Err(())
    } else {
        unsafe {Ok(mem::transmute::<_,guile::SCM>(eval).into()) }
    }
}

pub struct Any {}

pub trait All {}

impl All for Any {}

pub struct SCM<T: All + ?Sized> {
    pub gptr:  guile::SCM,
    pub _type: PhantomData<T>,
}

impl<T: All> Clone for SCM<T> {
    #[inline]
    fn clone(&self) -> Self {
        self.gptr.into()
    }
}

impl<T: All> Copy for SCM<T> {}

unsafe impl<T: All> marker::Send for SCM<T> {}
unsafe impl<T: All> marker::Sync for SCM<T> {}

#[cfg(feature = "better-dbg-fmt")]
lazy_static! {
    static ref RX_PATH: ::regex::Regex = ::regex::Regex::new(r"(\w+::)+").unwrap();
}

#[cfg(not(feature = "better-dbg-fmt"))]
impl<T: All> Debug for SCM<T> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "SCM<?>({:?})", self.gptr)
    }
}

#[cfg(feature = "better-dbg-fmt")]
impl<T: All> Debug for SCM<T> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let sty = unsafe { ::std::intrinsics::type_name::<T>() };

        write!(f, "SCM<{}>({:?})", RX_PATH.replace_all(sty, ""), self.gptr)
    }
}

#[cfg(feature = "better-display")]
default impl<T: All> Display for SCM<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let mut sp = SCM::<Port>::open_output_string();

        unsafe { guile::scm_call_2(FN_DISPLAY.gptr, self.gptr, sp.gptr) };

        let res = write!(f, "{}", String::from(sp.get_output_string()));
        sp.close_port();
        res
    }
}

impl<T: All> SCM<T> {
    #[inline]
    pub fn into_scm<Q>(self) -> SCM<Q>
        where Q: All,
              SCM<Q>: From<SCM<T>>
    {
        SCM::<Q>::from(self)
    }

    /*fn gc_protect(&self) {
        unsafe { guile::scm_gc_protect_object(self.gptr) };
    }
    
    fn gc_unprotect(&self) {
        unsafe { guile::scm_gc_unprotect_object(self.gptr) };
    }*/
}

/*impl<T: All + ?Sized> Drop for SCM<T> {
    fn drop(&mut self) {
        self.gc_unprotect();
    }
}*/

impl<T: All> From<guile::SCM> for SCM<T> {
    #[inline]
    fn from(ptr: guile::SCM) -> Self {
        SCM { gptr:  ptr,
            _type: PhantomData, }
    }
}

macro_rules! ImplAnyFor {
    ($($t:ty),*) => {
        $(
            impl From<SCM<$t>> for SCM<Any> {
                #[inline]
                fn from(v: SCM<$t>) -> SCM<Any> {
                    v.gptr.into()
                }
            }
        )*
    }
}

//todo complete list
ImplAnyFor!(bool, Integer, Rational, Real, Complex, String, char, Symbol);

pub trait TryAs<T: All> {
    #[inline]
    fn try_as(&self) -> Option<SCM<T>>;
}

impl<T: All> SCM<T> {
    #[inline]
    pub fn try_scm<Q>(&self) -> Option<SCM<Q>>
        where Q: All,
              SCM<T>: TryAs<Q>
    {
        SCM::<T>::try_as(self)
    }
}

impl<T: All> TryAs<Any> for SCM<T> {
    fn try_as(&self) -> Option<SCM<Any>> {
        Some(self.gptr.into())
    }
}

impl<T: All> SCM<T> {
    #[inline]
    pub fn dc_any(&self) -> SCM<Any> {
        self.gptr.into()
    }
}

// =========================
//     U t i l i t i e s
// =========================

pub unsafe fn write_str_to_malloc_mem<'a>(v: &'a str) -> *mut os::raw::c_char {
    let mem = libc::malloc(mem::size_of::<os::raw::c_char>() * v.len()) as *mut os::raw::c_char;

    slice::from_raw_parts_mut(mem, v.len()).iter_mut()
                                           .zip(v.bytes())
                                           .for_each(|(p, b)| *p = b as i8);

    mem
}

pub unsafe fn write_str_to_managed_mem<'a>(v: &'a str) -> *mut os::raw::c_char {
    let mem = guile::scm_gc_malloc(mem::size_of::<os::raw::c_char>() * v.len(),
                                   "str from rust".as_ptr() as *const i8)
              as *mut os::raw::c_char;

    slice::from_raw_parts_mut(mem, v.len()).iter_mut()
                                           .zip(v.bytes())
                                           .for_each(|(p, b)| *p = b as i8);

    mem
}

type Errcatch = Mutex<Cell<Option<Box<any::Any + marker::Send>>>>;

lazy_static! {
    static ref ECBODY: Errcatch = Mutex::new(Cell::new(None));
    static ref ECHANDLER: Errcatch = Mutex::new(Cell::new(None));
}

unsafe extern "C" fn cbody(data: *mut os::raw::c_void) -> guile::SCM {
    let closure: &mut &mut FnMut() -> () = mem::transmute(data);

    if let Err(payload) = panic::catch_unwind(panic::AssertUnwindSafe(closure)) {
        let ec = ECBODY.lock().unwrap();

        ec.set(Some(payload));

        return SCM::from(false).gptr;
    }

    SCM::from(true).gptr
}

unsafe extern "C" fn chandler(data: *mut os::raw::c_void,
                              key: guile::SCM,
                              args: guile::SCM)
                              -> guile::SCM
{
    let closure: &mut &mut FnMut(SCM<Symbol>, SCM<Any>) -> () = mem::transmute(data);

    if let Err(payload) = panic::catch_unwind(panic::AssertUnwindSafe(|| {
                                                                          closure(key.into(),
                                                                                  args.into());
                                                                      })) {
        let ec = ECHANDLER.lock().unwrap();

        ec.set(Some(payload));
    }

    key
}

pub fn internal_catch<Q: All>(tag: SCM<Q>,
                              mut body: &FnMut() -> (),
                              mut handler: &FnMut(SCM<Symbol>, SCM<Any>) -> ())
{
    unsafe {
        guile::scm_internal_catch(tag.gptr,
                                  Some(cbody),
                                  &mut body as *mut _ as *mut os::raw::c_void,
                                  Some(chandler),
                                  &mut handler as *mut _ as *mut os::raw::c_void);
    }

    if let Some(payload) = {
        let ecell = ECBODY.lock().unwrap();
        ecell.replace(None)
    } {
        panic::resume_unwind(payload);
    }

    if let Some(payload) = {
        let ecell = ECHANDLER.lock().unwrap();
        ecell.replace(None)
    } {
        panic::resume_unwind(payload);
    }
}
