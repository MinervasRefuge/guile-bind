#![allow(unused_macros)]
#![allow(unused_imports)]
#![allow(unused_variables)]
#![cfg_attr(feature = "better-dbg-fmt", feature(core_intrinsics))]
#![cfg_attr(feature = "better-display", feature(specialization))]
#![cfg_attr(feature = "proc-macro", feature(proc_macro))]

extern crate libc;
extern crate num;
extern crate num_complex;
extern crate num_rational;
extern crate qptrie;

#[macro_use]
extern crate lazy_static;
#[cfg(feature = "proc-macro")]
extern crate guile_bind_proc_macro;
#[cfg(feature = "better-dbg-fmt")]
extern crate regex;

mod guile;
mod number_operators;

pub mod bool;
pub mod char;
pub mod error;
pub mod keyword;
pub mod list;
pub mod number;
pub mod registrar;
pub mod scheme;
pub mod string;
pub mod symbol;

pub mod features;
pub mod hook;
pub mod port;

pub use guile::SCM as InternalSCM;
pub use scheme::{All, TryAs, SCM};

pub use list::List;
pub use number::{Complex, Integer, Rational, Real};
