use scheme;
use symbol;

use scheme::SCM;
use symbol::Symbol;

#[derive(Debug, Clone)]
pub struct ESCM {
    pub desc:    &'static str,
    pub errcode: &'static str,
    pub errsym:  SCM<Symbol>,
}

impl ESCM {
    fn new(desc: &'static str, errcode: &'static str) -> ESCM {
        ESCM { desc:    desc,
            errcode: errcode.clone(),
            errsym:  SCM::from(errcode), }
    }
}

macro_rules! error_build {
    ($($i:ident = $e:expr;)*) => {
        lazy_static! {
            $(static ref $i:ESCM = $e;)*
            static ref ERROR_LIST: Vec<&'static ESCM> = vec![$(&$i),*];
        }
    }
}

error_build! {
    EWRONG_ERROR = ESCM::new("Wrong Error", "wrong-error");
    EOUT_OF_RANGE = ESCM::new("Out of Range", "out-of-range");
}

pub fn error_match<'a, 'b>(m: &'a str) -> Option<&'b ESCM> {
    ERROR_LIST.iter().find(|&v| v.errcode == m).map(|&v| v)
}
