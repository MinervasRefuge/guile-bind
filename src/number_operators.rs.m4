divert(-1)

define(AddOp, `
impl Add<SCM<$2>> for SCM<$1> {
    type Output = SCM<$3>;

    fn add(self, rhs: SCM<$2>) -> Self::Output {
        unsafe { guile::scm_sum(self.gptr, rhs.gptr) }.into()
    }
}
dnl')

define(SubOp, `
impl Sub<SCM<$2>> for SCM<$1> {
    type Output = SCM<$3>;

    fn sub(self, rhs: SCM<$2>) -> Self::Output {
        unsafe { guile::scm_difference(self.gptr, rhs.gptr) }.into()
    }
}
dnl')
define(MulOp, `
impl Mul<SCM<$2>> for SCM<$1> {
    type Output = SCM<$3>;

    fn mul(self, rhs: SCM<$2>) -> Self::Output {
        unsafe { guile::scm_product(self.gptr, rhs.gptr) }.into()
    }
}
dnl')
define(DivOp, `
impl Div<SCM<$2>> for SCM<$1> {
    type Output = SCM<$3>;

    fn div(self, rhs: SCM<$2>) -> Self::Output {
        unsafe { guile::scm_divide(self.gptr, rhs.gptr) }.into()
    }
}
dnl')

divert(0)dnl
                /*                 WARNING                 */
                /* Don't edit this file as it is generated */
                /*        all changes will be lost         */
                //source: __file__

use scheme::SCM;
use number::*;
use guile;

use std::ops::{Add, Sub, Mul, Div};

// =============================================================================
//                                 A d d   O p 
// =============================================================================
AddOp(Integer,  Integer,        Integer)
AddOp(Rational, Rational,       Rational)
AddOp(Real,     Real,           Real)
AddOp(Complex,  Complex,        Complex)
dnl
AddOp(Integer,  Rational,       Rational)
AddOp(Integer,  Real,           Real)
AddOp(Integer,  Complex,        Complex)
dnl
AddOp(Rational, Integer,        Rational)
AddOp(Rational, Real,           Real)
AddOp(Rational, Complex,        Complex)
dnl
AddOp(Real,     Integer,        Real)
AddOp(Real,     Rational,       Real)
AddOp(Real,     Complex,        Complex)
dnl
AddOp(Complex,  Integer,        Complex)
AddOp(Complex,  Rational,       Complex)
AddOp(Complex,  Real,           Complex)

// =============================================================================
//                                 S u b   O p 
// =============================================================================
SubOp(Integer,  Integer,        Integer)
SubOp(Rational, Rational,       Rational)
SubOp(Real,     Real,           Real)
SubOp(Complex,  Complex,        Complex)
dnl
SubOp(Integer,  Rational,       Rational)
SubOp(Integer,  Real,           Real)
SubOp(Integer,  Complex,        Complex)
dnl
SubOp(Rational, Integer,        Rational)
SubOp(Rational, Real,           Real)
SubOp(Rational, Complex,        Complex)
dnl
SubOp(Real,     Integer,        Real)
SubOp(Real,     Rational,       Real)
SubOp(Real,     Complex,        Complex)
dnl
SubOp(Complex,  Integer,        Complex)
SubOp(Complex,  Rational,       Complex)
SubOp(Complex,  Real,           Complex)

// =============================================================================
//                                 M u l   O p 
// =============================================================================
MulOp(Integer,  Integer,        Integer)
MulOp(Rational, Rational,       Rational)
MulOp(Real,     Real,           Real)
MulOp(Complex,  Complex,        Complex)
dnl
MulOp(Integer,  Rational,       Rational)
MulOp(Integer,  Real,           Real)
MulOp(Integer,  Complex,        Complex)
dnl
MulOp(Rational, Integer,        Rational)
MulOp(Rational, Real,           Real)
MulOp(Rational, Complex,        Complex)
dnl
MulOp(Real,     Integer,        Real)
MulOp(Real,     Rational,       Real)
MulOp(Real,     Complex,        Complex)
dnl
MulOp(Complex,  Integer,        Complex)
MulOp(Complex,  Rational,       Complex)
MulOp(Complex,  Real,           Complex)

// =============================================================================
//                                 D i v   O p 
// =============================================================================
DivOp(Integer,  Integer,        Rational)
DivOp(Rational, Rational,       Rational)
DivOp(Real,     Real,           Real)
DivOp(Complex,  Complex,        Complex)
dnl
DivOp(Integer,  Rational,       Rational)
DivOp(Integer,  Real,           Real)
DivOp(Integer,  Complex,        Complex)
dnl
DivOp(Rational, Integer,        Rational)
DivOp(Rational, Real,           Real)
DivOp(Rational, Complex,        Complex)
dnl
DivOp(Real,     Integer,        Real)
DivOp(Real,     Rational,       Real)
DivOp(Real,     Complex,        Complex)
dnl
DivOp(Complex,  Integer,        Complex)
DivOp(Complex,  Rational,       Complex)
DivOp(Complex,  Real,           Complex)
