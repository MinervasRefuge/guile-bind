// =============================================================================
//                       §   6 . 6 . 1   B o o l e a n s
// =============================================================================

use features::*;
use guile;
use scheme;
use scheme::*;

use std::default::Default;
use std::marker::PhantomData;
use std::ops::{BitAnd, BitOr, BitXor, Not};
use std::sync::Mutex;
use std::*;

lazy_static! {
    static ref GUILE_BOOL_VALUE: Mutex<(SCM<bool>, SCM<bool>)> = {
        scheme::init_guile_for_current_thread();

        Mutex::new((scheme::eval_string("#f").unwrap(), scheme::eval_string("#t").unwrap()))
    };
}

impl From<bool> for SCM<bool> {
    fn from(v: bool) -> Self {
        let gb = GUILE_BOOL_VALUE.lock().unwrap();

        if v {
            gb.1
        } else {
            gb.0
        }
    }
}

impl From<SCM<bool>> for bool {
    fn from(v: SCM<bool>) -> Self {
        (unsafe { guile::scm_to_bool(v.gptr) } != 0)
    }
}

impl Not for SCM<bool> {
    type Output = Self;

    fn not(self) -> Self {
        unsafe { guile::scm_not(self.gptr) }.into()
    }
}

impl BitAnd for SCM<bool> {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        //todo is there a better way?

        (bool::from(self) & bool::from(rhs)).into()
    }
}

impl BitOr for SCM<bool> {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        //todo is there a better way?

        (bool::from(self) | bool::from(rhs)).into()
    }
}

impl BitXor for SCM<bool> {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output {
        //todo is there a better way?

        (bool::from(self) ^ bool::from(rhs)).into()
    }
}

impl Default for SCM<bool> {
    fn default() -> Self {
        bool::default().into()
    }
}

impl All for bool {}

impl<T: All> SCM<T> {
    pub fn is_bool_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_boolean_p(self.gptr) }.into()
    }

    pub fn is_bool(&self) -> bool {
        self.is_bool_scm().into()
    }
}

impl<T: All> TryAs<bool> for SCM<T> {
    fn try_as(&self) -> Option<SCM<bool>> {
        if self.is_bool() {
            Some(self.gptr.into())
        } else {
            None
        }
    }
}
