// =============================================================================
//                      §   6 . 6 . 3   C h a r a c t e r s
// =============================================================================

use guile;
use scheme::*;

use features::*;
use number::*;
use string::*;
use symbol::*;

use qptrie::Trie;

use std::convert::From;
use std::error::Error;
use std::*;

impl All for char {}

#[derive(Debug)]
struct CharRangeError {
    desc: &'static str,
}

impl Error for CharRangeError {
    fn description(&self) -> &str {
        self.desc
    }
}

impl fmt::Display for CharRangeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

//Unicode scalar value https://www.unicode.org/glossary/#unicode_scalar_value
impl TryFrom<SCM<char>> for char {
    type Error = Box<Error>;

    fn try_from(v: SCM<char>) -> Result<Self, Self::Error> {
        ::std::char::from_u32(v.into_scm::<Integer>()
                              .try_into()
                              .map_err(Box::new)?)
            .ok_or(CharRangeError { desc: "Out Of char range" }.into())
    }
}

//Unicode scalar value
impl From<char> for SCM<char> {
    fn from(v: char) -> Self {
        SCM::from(v as u32).into()
    }
}

impl From<SCM<Integer>> for SCM<char> {
    fn from(v: SCM<Integer>) -> Self {
        //todo error
        unsafe { guile::scm_integer_to_char(v.gptr) }.into()
    }
}

impl SCM<char> {
    pub fn is_alphabetic(&self) -> SCM<bool> {
        unsafe { guile::scm_char_alphabetic_p(self.gptr) }.into()
    }

    pub fn is_numeric(&self) -> SCM<bool> {
        unsafe { guile::scm_char_numeric_p(self.gptr) }.into()
    }

    pub fn is_whitespace(&self) -> SCM<bool> {
        unsafe { guile::scm_char_whitespace_p(self.gptr) }.into()
    }

    pub fn is_uppercase(&self) -> SCM<bool> {
        unsafe { guile::scm_char_upper_case_p(self.gptr) }.into()
    }

    pub fn is_lowercase(&self) -> SCM<bool> {
        unsafe { guile::scm_char_lower_case_p(self.gptr) }.into()
    }

    pub fn is_upper_or_lower_case(&self) -> SCM<bool> {
        unsafe { guile::scm_char_is_both_p(self.gptr) }.into()
    }

    pub fn general_category(&self) -> Option<UnicodeGeneralCategory> {
        let v: SCM<Any> = unsafe { guile::scm_char_general_category(self.gptr) }.into();

        if let Some(sc) = v.try_scm::<Symbol>() {
            let r = String::from(sc);

            UnicodeGeneralCategory::find(r.as_str())
        } else {
            None
        }
    }

    pub fn to_uppercase(&self) -> SCM<char> {
        unsafe { guile::scm_char_upcase(self.gptr) }.into()
    }

    pub fn to_lowercase(&self) -> SCM<char> {
        unsafe { guile::scm_char_downcase(self.gptr) }.into()
    }
}

impl<T: All> SCM<T> {
    pub fn is_char_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_char_p(self.gptr) }.into()
    }

    pub fn is_char(&self) -> bool {
        self.is_char_scm().into()
    }
}

impl<T: All> TryAs<char> for SCM<T> {
    fn try_as(&self) -> Option<SCM<char>> {
        if self.is_char() {
            Some(self.gptr.into())
        } else {
            None
        }
    }
}

macro_rules! build_UGC {
    ($name:ident ($($cat:ident = $s:expr),*)) => {
        lazy_static! {
            // too excessive using a trie here?
            static ref UGC_REF: Trie<&'static str, $name> = {
                let mut t = Trie::new();

                $(t.insert($s, $name::$cat);)*

                t
            };
        }

        #[derive(Clone, Copy, Debug, PartialEq, Eq)]
        pub enum $name {$(
            $cat
        ),*}

        impl $name {
            pub fn value(&self) -> &'static str {
                match *self {$(
                    $name::$cat => $s
                ),*}
            }

            pub fn find(s: &str) -> Option<$name> {
                UGC_REF.get(&s).map(|&v| v)
            }
        }
    }
}

build_UGC!{
    UnicodeGeneralCategory (
        UppercaseLetter = "Lu",
        LowercaseLetter = "Ll",
        TitlecaseLetter = "Lt",
        ModifierLetter  = "Lm",
        OtherLetter     = "Lo",

        NonspaceMark       = "Mn",
        CombiningSpaceMark = "Mc",
        EnclosingMark      = "Me",

        DecimalDigitNumber = "Nd",
        LetterNumber       = "Nl",
        OtherNumber        = "No",

        ConnectorPunctuation    = "Pc",
        DashPunctuation         = "Pd",
        OpenPunctuation         = "Ps",
        ClosePunctuation        = "Pe",
        InitialQuotePunctuation = "Pi",
        FinalQuotePunctuation   = "Pf",
        OtherPunctuation        = "Po",

        MathSymbol     = "Sm",
        CurrencySymbol = "Sc",
        ModifierSymbol = "Sk",
        OtherSymbol    = "So",

        SpaceSeperator     = "Zs",
        LineSeperator      = "Zl",
        ParagraphSeperator = "Zp",

        Control    = "Cc",
        Format     = "Cf",
        Surrogate  = "Cs",
        PrivateUse = "Co",
        Unassigned = "Cn"
    )
}
