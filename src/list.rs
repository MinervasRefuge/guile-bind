// =============================================================================
//                           §   6 . 6 . 9   L i s t s
// =============================================================================

use guile;

use features::*;
use number::*;
use scheme;
use scheme::*;

use std::ffi::CString;
use std::iter::DoubleEndedIterator;
use std::marker::PhantomData;
use std::ops::Index;
use std::*;

lazy_static! {
    static ref GUILE_END_OF_LIST: SCM<Any> = {
        scheme::init_guile_for_current_thread();
        scheme::eval_string("'()").unwrap()
    };
}

pub struct List<T: All> {
    _type: PhantomData<T>,
}

impl<T: All> All for List<T> {}

impl<T: All> SCM<T> {
    pub fn list<Q>(l: Q) -> SCM<List<T>>
        where Q: IntoIterator<Item = SCM<T>> {
        let mut head: Option<guile::SCM> = None;
        let mut last: Option<guile::SCM> = None;

        for e in l {
            let pair = unsafe { guile::scm_list_1(e.gptr) };

            if head.is_none() {
                head = Some(pair);
            }

            match last {
                None => last = Some(pair),
                Some(currp) => {
                    unsafe { guile::scm_set_cdr_x(currp, pair) };
                    last = Some(pair);
                }
            }
        }

        match head {
            None => GUILE_END_OF_LIST.gptr,
            Some(v) => v,
        }.into()
    }

    pub fn is_list_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_list_p(self.gptr) }.into()
    }

    pub fn is_list(&self) -> bool {
        self.is_list_scm().into()
    }
}

impl<T: All, Q: All> TryAs<List<T>> for SCM<Q> {
    fn try_as(&self) -> Option<SCM<List<T>>> {
        //todo complete
        Some(self.gptr.into())
    }
}

impl<T: All> SCM<List<T>> {
    pub fn cons(&self, v: SCM<T>) -> SCM<List<T>> {
        unsafe { guile::scm_cons(v.gptr, self.gptr) }.into()
    }

    pub fn len_scm(&self) -> SCM<Integer> {
        unsafe { guile::scm_length(self.gptr) }.into()
    }

    pub fn len(&self) -> usize {
        self.len_scm().try_into().unwrap()
    }

    pub fn get(&self, index: SCM<Integer>) -> SCM<T> {
        unsafe { guile::scm_list_ref(self.gptr, index.gptr) }.into()
    }

    pub fn last_pair(&self) -> SCM<T> {
        unsafe { guile::scm_last_pair(self.gptr) }.into()
    }

    pub fn list_tail(&self, k: SCM<Integer>) -> SCM<List<T>> {
        //todo error checking
        unsafe { guile::scm_list_tail(self.gptr, k.gptr) }.into()
    }

    pub fn list_head(&self, k: SCM<Integer>) -> SCM<List<T>> {
        //todo error checking
        unsafe { guile::scm_list_head(self.gptr, k.gptr) }.into()
    }

    pub fn car(&self) -> Option<SCM<T>> {
        //todo error checking
        Some(unsafe { guile::scm_car(self.gptr) }.into())
    }

    pub fn cdr(&self) -> Option<SCM<List<T>>> {
        //todo error checking
        Some(unsafe { guile::scm_cdr(self.gptr) }.into())
    }

    #[inline]
    pub fn append(&self, v: &[SCM<T>]) -> SCM<List<T>> {
        self.append_any(unsafe { mem::transmute::<&[SCM<T>], &[SCM<Any>]>(v) }).gptr
            .into()
    }

    pub fn append_any(&self, v: &[SCM<Any>]) -> SCM<List<Any>> {
        unsafe {
            guile::scm_append(
                SCM::list([self.gptr.into(), SCM::list(v.iter().cloned())]
                          .iter().cloned())
                    .gptr)
        }.into()
    }

    pub fn xappend(&mut self, v: &[SCM<T>]) -> SCM<List<T>> {
        unsafe {
            guile::scm_append_x(
                SCM::list([*self, SCM::list(v.iter().cloned())]
                          .iter().cloned())
                    .gptr)
        }.into()
    }

    pub fn reverse(&self) -> SCM<List<T>> {
        unsafe { guile::scm_reverse(self.gptr) }.into()
    }

    pub fn xreverse(&mut self) -> SCM<List<T>> {
        unsafe { guile::scm_reverse_x(self.gptr, GUILE_END_OF_LIST.gptr) }.into()
    }

    pub fn xset(&mut self, index: SCM<Integer>, v: &SCM<T>) {
        unsafe { guile::scm_list_set_x(self.gptr, index.gptr, v.gptr) };
    }

    pub fn delq<Q: All>(&self, d: SCM<Q>) -> SCM<List<T>> {
        unsafe { guile::scm_delq(d.gptr, self.gptr) }.into()
    }

    pub fn xdelq(&mut self, d: SCM<Any>) -> SCM<List<T>> {
        unsafe { guile::scm_delq_x(d.gptr, self.gptr) }.into()
    }

    pub fn delv<Q: All>(&self, d: SCM<Q>) -> SCM<List<T>> {
        unsafe { guile::scm_delv(d.gptr, self.gptr) }.into()
    }

    pub fn xdelv(&mut self, d: SCM<Any>) -> SCM<List<T>> {
        unsafe { guile::scm_delv_x(d.gptr, self.gptr) }.into()
    }

    pub fn delete<Q: All>(&self, d: SCM<Q>) -> SCM<List<T>> {
        unsafe { guile::scm_delete(d.gptr, self.gptr) }.into()
    }

    pub fn xdelete(&mut self, d: SCM<Any>) -> SCM<List<T>> {
        unsafe { guile::scm_delete_x(d.gptr, self.gptr) }.into()
    }

    pub fn xdelq1(&mut self, d: SCM<Any>) -> SCM<List<T>> {
        unsafe { guile::scm_delv1_x(d.gptr, self.gptr) }.into()
    }

    pub fn xdelv1(&mut self, d: SCM<Any>) -> SCM<List<T>> {
        unsafe { guile::scm_delq1_x(d.gptr, self.gptr) }.into()
    }

    pub fn xdelete1(&mut self, d: SCM<Any>) -> SCM<List<T>> {
        unsafe { guile::scm_delete1_x(d.gptr, self.gptr) }.into()
    }

    //todo §6.6.9.7 List Searching
    //todo §6.6.9.8 List Mapping

    pub fn iter(&self) -> SCMIterList<T> {
        SCMIterList { list:       &self,
            index:      0,
            index_back: self.len(), }
    }

    pub fn is_empty_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_null_p(self.gptr) }.into()
    }

    pub fn is_empty(&self) -> bool {
        self.is_empty_scm().into()
    }
}

pub struct SCMIterList<'a, T: All + 'a> {
    list:       &'a SCM<List<T>>,
    index:      usize,
    index_back: usize,
}

impl<'a, T: All> Iterator for SCMIterList<'a, T> {
    type Item = SCM<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.index_back {
            return None;
        }

        let res = self.list.get(self.index.into());
        self.index += 1;
        Some(res)
    }
}

impl<'a, T: All> DoubleEndedIterator for SCMIterList<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.index >= self.index_back {
            return None;
        }

        self.index_back -= 1;
        Some(self.list.get(self.index_back.into()))
    }
}

impl<'a, T: All> ExactSizeIterator for SCMIterList<'a, T> {
    fn len(&self) -> usize {
        self.index_back - self.index
    }
}

pub struct SCMIntoIter<I: All, Q: Iterator> {
    iter:  Q,
    _type: PhantomData<I>,
}

pub struct SCMOutOfIter<T: All, Q: Iterator<Item = SCM<T>>> {
    iter:  Q,
    _type: PhantomData<SCM<T>>,
}

pub trait SCMChangeIntoIter<Q: Iterator> {
    fn into_scm<I: All>(self) -> SCMIntoIter<I, Q>
        where SCM<I>: From<Q::Item>;
}

pub trait SCMChangeOutOfIter<T: All, Q: Iterator<Item = SCM<T>>> {
    fn out_of_scm(self) -> SCMOutOfIter<T, Q>;
}

impl<Q: Iterator> SCMChangeIntoIter<Q> for Q {
    fn into_scm<I: All>(self) -> SCMIntoIter<I, Q>
        where SCM<I>: From<Q::Item> {
        SCMIntoIter { iter:  self,
            _type: PhantomData, }
    }
}

impl<T: All, Q: Iterator<Item = SCM<T>>> SCMChangeOutOfIter<T, Q> for Q {
    fn out_of_scm(self) -> SCMOutOfIter<T, Q> {
        SCMOutOfIter { iter:  self,
            _type: PhantomData, }
    }
}

impl<I: All, Q: Iterator> Iterator for SCMIntoIter<I, Q>
    where SCM<I>: From<Q::Item>
{
    type Item = SCM<I>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(SCM::<I>::from)
    }
}

impl<T: All + TryFrom<SCM<T>>, Q: Iterator<Item = SCM<T>>> Iterator for SCMOutOfIter<T, Q> {
    type Item = Result<T, <T as TryFrom<SCM<T>>>::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(T::try_from)
    }
}

/*impl<T: All> Clone for SCM<List<T>> {
    fn clone(&self) -> Self {
        unsafe { guile::scm_list_copy(self.gptr) }.into()
    }
}*/
