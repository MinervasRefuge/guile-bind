use std::fmt;
use std::marker::Sized;

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Infallible {}

impl fmt::Display for Infallible {
    fn fmt(&self, _: &mut fmt::Formatter) -> fmt::Result {
        match *self {}
    }
}

pub trait TryFrom<T> {
    type Error;

    fn try_from(T) -> Result<Self, Self::Error>
        where Self: Sized;
}

impl<Q, T: From<Q>> TryFrom<Q> for T {
    type Error = Infallible;

    #[inline]
    fn try_from(v: Q) -> Result<Self, Self::Error> {
        Ok(T::from(v))
    }
}

pub trait TryInto<T> {
    type Error;

    fn try_into(self) -> Result<T, Self::Error>;
}

impl<T, Q> TryInto<Q> for T
    where Q: TryFrom<T>
{
    type Error = <Q as TryFrom<T>>::Error;

    #[inline]
    fn try_into(self) -> Result<Q, Self::Error> {
        Q::try_from(self)
    }
}
