#[cfg(not(feature = "common_try"))]
mod stry_from;
#[cfg(not(feature = "common_try"))]
pub use self::stry_from::{TryFrom, TryInto};

#[cfg(feature = "common_try")]
pub use std::convert::{TryFrom, TryInto};
