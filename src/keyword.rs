// =============================================================================
//                         §   6 . 6 . 7   K e y w o r d
// =============================================================================

use features::*;
use guile;
use scheme;
use scheme::*;
use symbol::Symbol;

use std::default::Default;
use std::marker::PhantomData;
use std::*;

pub struct Keyword {}

impl All for Keyword {}

impl From<SCM<Symbol>> for SCM<Keyword> {
    fn from(v: SCM<Symbol>) -> Self {
        unsafe { guile::scm_symbol_to_keyword(v.gptr) }.into()
    }
}

impl From<String> for SCM<Keyword> {
    fn from(v: String) -> Self {
        SCM::<Keyword>::from(v.as_str())
    }
}

impl<'a> From<&'a str> for SCM<Keyword> {
    fn from(v: &'a str) -> Self {
        unsafe {
            guile::scm_from_locale_keywordn(
                scheme::write_str_to_malloc_mem(v),
                v.len())
        }.into()
    }
}

impl<T: All> SCM<T> {
    pub fn is_keyword_scm(&self) -> SCM<bool> {
        unsafe { guile::scm_keyword_p(self.gptr) }.into()
    }

    pub fn is_keyword(&self) -> bool {
        self.is_keyword_scm().into()
    }
}

impl Default for SCM<Keyword> {
    fn default() -> Self {
        String::default().into()
    }
}
