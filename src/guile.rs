#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

use std::os::raw::*;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

extern "C" {
    pub fn scm_is_string(obj: SCM) -> c_int;
    pub fn scm_cons(x: SCM, y: SCM) -> SCM;
    pub fn scm_car(p: SCM) -> SCM;
    pub fn scm_cdr(p: SCM) -> SCM;
}
