extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rustc-link-lib=guile-2.2");
    println!("cargo:rerun-if-changed=./src/wrapper.h");

    let bindings = bindgen::Builder::default().header("./src/wrapper.h")
                                              .blacklist_type("max_align_t")
                                              .layout_tests(false)
                                              .clang_arg("-I/usr/include/guile/2.2/")
                                              .generate()
                                              .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings.write_to_file(out_path.join("bindings.rs"))
            .expect("Couldn't write bindings!");
}
