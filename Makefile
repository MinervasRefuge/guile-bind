.PHONY: all loc

M4GENS_RS = $(patsubst src/%.rs.m4, src/%.rs, $(wildcard src/*.rs.m4))

all: $(M4GENS_RS)
	cargo build --release

src/%: src/%.m4
	m4 $< > $@

clean:
	cargo clean
	rm $(M4GENS_RS)

test: $(M4GENS_RS)
	cargo test -- --test-threads=1

loc:
	tokei ./src ./tests ./examples
