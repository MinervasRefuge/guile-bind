#[macro_use]
extern crate guile_bind;
#[macro_use]
extern crate lazy_static;

use guile_bind::features::*;
use guile_bind::scheme::Any;
use guile_bind::*;
use std::cell::Cell;
use std::sync::Mutex;

lazy_static! {
    static ref FIB_NUM: Mutex<Cell<Vec<Option<u64>>>> = {
        let mut v = Vec::new();

        v.push(Some(0));
        v.push(Some(1));
        v.push(Some(1));

        Mutex::new(v.into())
    };
    static ref NUM_ZERO: SCM<Integer> = 0.into();
    static ref NUM_ONE: SCM<Integer> = 1.into();
    static ref NUM_TWO: SCM<Integer> = 2.into();
}

scmFn! {
    fn internal_rcache_fib | rcache_fib (n: SCM<Integer>) -> SCM<Integer> {
        let mut m = FIB_NUM.lock().unwrap();

        rfib(n.try_into().unwrap(), m.get_mut()).into()
    }
}

fn rfib(n: usize, fib_cache: &mut Vec<Option<u64>>) -> u64 {
    if fib_cache.len() <= n {
        for _ in fib_cache.len()..(n + 1) {
            fib_cache.push(None);
        }
    }

    if let Some(v) = fib_cache[n] {
        v
    } else {
        let v = rfib(n - 1, fib_cache) + rfib(n - 2, fib_cache);
        fib_cache[n] = Some(v);
        v
    }
}

scmFn! {
    fn internal_simple_fib | simple_fib (n: SCM<Integer>) -> SCM<Integer> {
        if n == *NUM_ZERO {
            *NUM_ZERO
        } else if n == *NUM_ONE {
            *NUM_ONE
        } else if n == *NUM_TWO {
            *NUM_ONE
        } else {
            (simple_fib(n - *NUM_ONE) + simple_fib(n - *NUM_TWO))
        }
    }
}

scmFn! {
    fn internal_scache_fib | scache_fib
        (n: SCM<Integer>, fcache: SCM<List<Integer>>) -> SCM<Integer> {
            if fcache.len_scm() <= n {
                let v = scache_fib(n - *NUM_ONE, fcache) + scache_fib(n - *NUM_TWO, fcache);
                fcache.xappend(&[v]);

                v
            } else {
                fcache.get(n)
            }
        }
}

fn main() {
    scheme::init_guile_for_current_thread();
    registrar::register_scm_fn1("rcache-fib", internal_rcache_fib);
    registrar::register_scm_fn1("simple-fib", internal_simple_fib);
    registrar::register_scm_fn2("scache-fib", internal_scache_fib);

    println!("rcache:\t{}",
             scheme::eval_string::<Integer>("(rcache-fib 33)").unwrap());
    println!("simple:\t{}",
             scheme::eval_string::<Integer>("(simple-fib 33)").unwrap());
    println!("scache:\t{}",
             scheme::eval_string::<Integer>("(scache-fib 33 '(0 1 1))").unwrap());
}
